import fr.irit.smac.amas4dc.AMAS4DC
import fr.irit.smac.amas4dc.amas.AMASOption
import fr.irit.smac.amas4dc.amas.MASSettings
import fr.irit.smac.amas4dc.cluster.Cluster
import fr.irit.smac.amas4dc.cluster.datapoint.DataPoint
import fr.irit.smac.amas4dc.cluster.distance.DistanceMethod
import fr.irit.smac.lxplot.LxPlot
import fr.irit.smac.lxplot.commons.ChartType
import groovy.transform.ToString

import java.awt.geom.Point2D
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime

class TwoDimensionsPointsClustering {
	static void main(String[] args) {

		def amas4dc = new AMAS4DC<TwoDimensionsDataPoint>(new MASSettings(new TwoDimensionsDistanceMethod(), 0.5f, EnumSet.noneOf(AMASOption), (TwoDimensionsDataPoint dp1, TwoDimensionsDataPoint dp2) -> {
			return new TwoDimensionsDataPoint(new Point2D.Double((dp1.getPoint2D().getX() + dp2.getPoint2D().getX()) / 2,
			                                                     (dp1.getPoint2D().getY() + dp2.getPoint2D().getY()) / 2))
		}))

		LxPlot.getChart("Data", ChartType.PLOT);
		def initialData = [new TwoDimensionsDataPoint(new Point2D.Double(1.0, 1.0)), new TwoDimensionsDataPoint(new Point2D.Double(2.0, 2.0))]
		render(initialData)
		amas4dc.fit(initialData)
		var results = amas4dc.retrieveClusters()
		println(results)
		render(results.clusters())
	}

	static void render(ArrayList<TwoDimensionsDataPoint> twoDimensionsDataPoints) {
		for (def tddp : twoDimensionsDataPoints) {
			LxPlot.getChart("Data").add("Initial", tddp.point2D.getX(), tddp.getPoint2D().getY())
		}
	}

	static void render(List<Cluster<TwoDimensionsDataPoint>> clusters) {
		for (def c : clusters) {
			LxPlot.getChart("Data").add("Cluster representative point", c.getRepresentative().point2D.getX(), c.getRepresentative().point2D.getY())
		}
	}

	@ToString
	static class TwoDimensionsDataPoint implements DataPoint {
		Point2D.Double point2D
		LocalDateTime time

		TwoDimensionsDataPoint(Point2D.Double point2D) {
			this.point2D = point2D
			this.time = LocalDateTime.now()
		}
		@Override
		LocalTime getEventTime() {
			return this.time.toLocalTime()
		}

		@Override
		LocalDate getEventDate() {
			return this.time.toLocalDate()
		}
	}

	static class TwoDimensionsDistanceMethod implements DistanceMethod<TwoDimensionsDataPoint> {

		@Override
		float apply(TwoDimensionsDataPoint dp1, TwoDimensionsDataPoint dp2) {
			if (dp1.point2D.distance(dp2.point2D) < 10)
				return 0
			return 1
		}

		@Override
		boolean areRoughlySimilar(TwoDimensionsDataPoint dp1, TwoDimensionsDataPoint dp2) {
			return true
		}
	}
}
