package fr.irit.smac.amas4dc.cluster.evaluation;

import fr.irit.smac.amas4dc.amas.MASSettings;
import fr.irit.smac.amas4dc.cluster.ExtendedCluster;
import fr.irit.smac.amas4dc.cluster.datapoint.DataPoint;

import java.util.List;

public class CalinskiHarabazIndex<T extends DataPoint> {

	public double compute(MASSettings<T> masSettings, List<ExtendedCluster<T>> clusters) {
		var ssb = computeBetweenClusterVariance(clusters, masSettings);
		var ssw = computeWithinClusterVariance(clusters, masSettings);

		if (ssw == 0) {
			return Double.POSITIVE_INFINITY;
		}

		return ssb / ssw;
	}

	private double computeBetweenClusterVariance(List<ExtendedCluster<T>> clusters, MASSettings<T> masSettings) {
		var centroid = computeGlobalCentroid(clusters, masSettings);
		var ssb = 0.0;

		for (var cluster : clusters) {
			var centroidDist = masSettings.distanceMethod().apply(cluster.getRepresentative(), centroid);
			ssb += centroidDist * centroidDist * cluster.getContent().size();
		}

		return ssb;
	}

	private double computeWithinClusterVariance(List<ExtendedCluster<T>> clusters, MASSettings<T> masSettings) {
		var ssw = 0.0;

		for (var cluster : clusters) {
			var clusterCentroid = cluster.getRepresentative();
			var clusterSumDist = 0.0;

			for (var point : cluster.getContent()) {
				var dist = masSettings.distanceMethod().apply(point, clusterCentroid);
				clusterSumDist += dist * dist;
			}

			ssw += clusterSumDist;
		}

		return ssw;
	}

	private T computeGlobalCentroid(List<ExtendedCluster<T>> clusters, MASSettings<T> masSettings) {
		var fused = clusters.get(0).getRepresentative();
		for (int i = 1; i < clusters.size(); i++) {
			masSettings.dataPointFuser().accept(fused, clusters.get(i).getRepresentative());
		}
		return fused;
	}
}

