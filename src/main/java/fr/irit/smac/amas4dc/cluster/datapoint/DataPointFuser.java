package fr.irit.smac.amas4dc.cluster.datapoint;

import java.util.function.BiConsumer;
import java.util.function.BiFunction;

public interface DataPointFuser<T extends DataPoint> extends BiConsumer<T, T> {
}
