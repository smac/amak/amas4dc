package fr.irit.smac.amas4dc.cluster;

import fr.irit.smac.amas4dc.cluster.datapoint.DataPoint;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
public class Cluster<T extends DataPoint> {
	@Getter
	private final T representative;
	@Getter
	private int size;

	public Cluster(T representative) {
		this.representative = representative;
		this.size = 1;
	}

	public void addDataPoint(T dataPoint) {
		size++;
	}

	public void addClusterContent(Cluster<T> otherCluster) {
		size += otherCluster.size;
	}
}

