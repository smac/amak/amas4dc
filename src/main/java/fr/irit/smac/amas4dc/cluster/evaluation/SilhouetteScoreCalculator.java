package fr.irit.smac.amas4dc.cluster.evaluation;

import fr.irit.smac.amas4dc.amas.AMASOption;
import fr.irit.smac.amas4dc.amas.MASSettings;
import fr.irit.smac.amas4dc.amas.controller.MissingDataPointsException;
import fr.irit.smac.amas4dc.cluster.ExtendedCluster;
import fr.irit.smac.amas4dc.cluster.datapoint.DataPoint;

import java.util.ArrayList;
import java.util.List;

public class SilhouetteScoreCalculator<T extends DataPoint> {
	public double compute(MASSettings<T> masSettings, List<ExtendedCluster<T>> clusters) {
		if (!masSettings.amasOptions().contains(AMASOption.KeepAllDataPoints))
			throw new MissingDataPointsException("AMAS option KeepAllDataPoints must be set to compute silhouette distance");
		var ss = 0.0;
		if (clusters.isEmpty())
			return ss;

		for (var c :
				clusters) {
			List<ExtendedCluster<T>> otherClusters = clusters.stream().filter(cl -> cl != c).toList();
			var outOfClusterDataPoints = new ArrayList<T>();
			for (var l : otherClusters) {
				outOfClusterDataPoints.addAll(l.getContent());
			}
			double silhouetteScore = computeOneClusterAgainstOthers(masSettings, c, outOfClusterDataPoints);
			ss += silhouetteScore;
		}
		return ss / clusters.size();

	}

	private double computeOneClusterAgainstOthers(MASSettings<T> masSettings, ExtendedCluster<T> cluster, List<T> outOfClusterDataPoints) {
		if (outOfClusterDataPoints.size() == 0) {
			return 1;
		}
		var ss = 0.0;
		if (cluster.getSize() > 0 && outOfClusterDataPoints.size() > 0) {
			for (var dataPoint : cluster.getContent()) {
				var a = averageDistanceTo(masSettings, dataPoint, cluster.getContent());
				var b = averageDistanceTo(masSettings, dataPoint, outOfClusterDataPoints);
				ss += (b - a) / Math.max(a, b);
			}
			ss /= cluster.getSize();
		}
		return ss;
	}

	private double averageDistanceTo(MASSettings<T> masSettings, T dataPoint, List<T> otherDataPoints) {
		if (otherDataPoints.size() == 1 && otherDataPoints.get(0) == dataPoint)
			return 0;
		var distance = otherDataPoints.stream().filter(i -> i != dataPoint).mapToDouble(i2 -> masSettings.distanceMethod().apply(dataPoint, i2)).average().getAsDouble();

		return distance;
	}


}
