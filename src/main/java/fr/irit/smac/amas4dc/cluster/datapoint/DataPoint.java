package fr.irit.smac.amas4dc.cluster.datapoint;

import java.time.LocalDate;
import java.time.LocalTime;

public interface DataPoint {
	default int getBucketId() {
		return 0;
	}

	LocalTime getEventTime();

	LocalDate getEventDate();
}
