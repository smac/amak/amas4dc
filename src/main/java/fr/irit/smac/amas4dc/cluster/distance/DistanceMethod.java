package fr.irit.smac.amas4dc.cluster.distance;


import fr.irit.smac.amas4dc.cluster.datapoint.DataPoint;

public interface DistanceMethod<T extends DataPoint> {
	float apply(T dp1, T dp2);
	boolean areRoughlySimilar(T dp1, T dp2);
}
