package fr.irit.smac.amas4dc.cluster.exception;

public class ExtendedClusterException extends RuntimeException {
	public ExtendedClusterException(String s) {
		super(s);
	}
}
