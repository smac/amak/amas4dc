package fr.irit.smac.amas4dc.cluster.datapoint;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DataPointDatabase<T extends DataPoint> {
	private static final Logger logger = Logger.getLogger(DataPointDatabase.class.getName());
	private final Set<T> content = new HashSet<>();
	private T lastModifiedDataPoint;

	public void add(T newDataPoint) {
		if (logger.isLoggable(Level.INFO))
			logger.info("Adding an element to database");
		content.add(newDataPoint);
	}

	public void remove(T dataPoint) {
		content.remove(dataPoint);
	}

	public Set<T> get() {
		return content;
	}

	public boolean contains(T dataPoint) {
		return content.contains(dataPoint);
	}

	public void setLastModifiedDataPoint(T dataPoint) {

		this.lastModifiedDataPoint = dataPoint;
	}
}
