package fr.irit.smac.amas4dc.cluster;

import fr.irit.smac.amas4dc.cluster.datapoint.DataPoint;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

public class ExtendedCluster<T extends DataPoint> extends Cluster<T> {
	@Getter
	private final List<T> content = new ArrayList<>();

	public ExtendedCluster(T representative) {
		super(representative);
		this.content.add(representative);
	}

	@Override
	public void addDataPoint(T dataPoint) {
		super.addDataPoint(dataPoint);
		content.add(dataPoint);
	}

	public void addClusterContent(ExtendedCluster<T> otherCluster) {
		for (var dp : otherCluster.getContent()) {
			addDataPoint(dp);
		}
	}
}
