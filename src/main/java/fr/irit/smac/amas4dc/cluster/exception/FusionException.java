package fr.irit.smac.amas4dc.cluster.exception;

public class FusionException extends RuntimeException {
	public FusionException(String message) {
		super(message);
	}
}
