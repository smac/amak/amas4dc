package fr.irit.smac.amas4dc.cluster.distance;


import fr.irit.smac.amas4dc.cluster.Cluster;
import fr.irit.smac.amas4dc.cluster.datapoint.DataPoint;

public record ClusterDistance<T extends DataPoint>(Cluster<T> cluster, float distance) {

}
