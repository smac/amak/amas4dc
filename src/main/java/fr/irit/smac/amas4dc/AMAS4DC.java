package fr.irit.smac.amas4dc;

import fr.irit.smac.amas4dc.amas.MASSettings;
import fr.irit.smac.amas4dc.amas.controller.AMAS4DCCommandAndQueryHandler;
import fr.irit.smac.amas4dc.amas.controller.command.NewDataPointCommand;
import fr.irit.smac.amas4dc.amas.controller.command.SolveCommand;
import fr.irit.smac.amas4dc.amas.controller.query.ExtendedResult;
import fr.irit.smac.amas4dc.amas.controller.query.Result;
import fr.irit.smac.amas4dc.amas.controller.query.RetrieveClustersExtendedResultQuery;
import fr.irit.smac.amas4dc.amas.controller.query.RetrieveClustersResultQuery;
import fr.irit.smac.amas4dc.cluster.datapoint.DataPoint;

import java.util.List;

public class AMAS4DC<T extends DataPoint> {
	private final AMAS4DCCommandAndQueryHandler<T> controller;

	public AMAS4DC(MASSettings<T> masSettings) {
		this.controller = new AMAS4DCCommandAndQueryHandler<T>(masSettings);
	}

	public void fit(List<T> data) {
		controller.handle(new NewDataPointCommand<T>(data));
		controller.handle(new SolveCommand());
	}

	public Result<T> retrieveClusters() {
		return controller.handle(new RetrieveClustersResultQuery());
	}

	public ExtendedResult<T> retrieveExtendedClusters() {
		return controller.handle(new RetrieveClustersExtendedResultQuery());
	}
}
