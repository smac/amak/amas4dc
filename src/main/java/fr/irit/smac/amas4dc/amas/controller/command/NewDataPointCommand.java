package fr.irit.smac.amas4dc.amas.controller.command;

import fr.irit.smac.amas4dc.cluster.datapoint.DataPoint;

import java.util.List;

public record NewDataPointCommand<T extends DataPoint>(List<T> newDataPoints) implements Command {
}
