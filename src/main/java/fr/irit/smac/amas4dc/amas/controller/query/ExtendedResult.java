package fr.irit.smac.amas4dc.amas.controller.query;

import fr.irit.smac.amas4dc.amas.MASSettings;
import fr.irit.smac.amas4dc.cluster.datapoint.DataPoint;
import fr.irit.smac.amas4dc.cluster.ExtendedCluster;

import java.util.List;


public record ExtendedResult<T extends DataPoint>(List<ExtendedCluster<T>> clusters,
                                                  double silhouetteScore,
                                                  int amountOfClustersWithFrequencyEqualToOne,
                                                  int amountOfClustersWithFrequencyHigherThan5,
                                                  MASSettings<T> masSettings) {
	public ExtendedResult(List<ExtendedCluster<T>> clusters, double silhouetteScore, MASSettings<T> masSettings) {
		this(clusters,
		     silhouetteScore,
		     (int) clusters.stream().filter(l -> l.getSize() == 1).count(),
		     (int) clusters.stream().filter(l -> l.getSize() > 5).count(),
		     masSettings);
	}
}
