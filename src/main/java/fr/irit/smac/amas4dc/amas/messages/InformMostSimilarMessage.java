package fr.irit.smac.amas4dc.amas.messages;

import fr.irit.smac.amak.Agent;
import fr.irit.smac.amak.messaging.Message;
import fr.irit.smac.amas4dc.amas.agent.ClusterAgent;
import fr.irit.smac.amas4dc.cluster.Cluster;
import fr.irit.smac.amas4dc.cluster.datapoint.DataPoint;
import lombok.Getter;

import java.util.List;

public class InformMostSimilarMessage<T extends DataPoint> extends Message {
	@Getter
	private final T dataPoint;
	@Getter
	private final List<ClusterAgent<T>> otherSimilarClusterAgents;
	@Getter
	private final Cluster<T> cluster;

	public InformMostSimilarMessage(Agent sender, T dataPoint, List<ClusterAgent<T>> otherSimilarClusterAgents) {
		super(sender);
		this.dataPoint = dataPoint;
		this.cluster = null;
		this.otherSimilarClusterAgents = otherSimilarClusterAgents;
	}
	public InformMostSimilarMessage(Agent sender, Cluster<T> cluster, List<ClusterAgent<T>> otherSimilarClusterAgents) {
		super(sender);
		this.cluster = cluster;
		this.dataPoint = null;
		this.otherSimilarClusterAgents = otherSimilarClusterAgents;
	}
}
