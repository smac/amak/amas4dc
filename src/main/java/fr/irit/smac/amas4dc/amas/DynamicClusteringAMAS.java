package fr.irit.smac.amas4dc.amas;

import fr.irit.smac.amak.Amas;
import fr.irit.smac.amas4dc.amas.agent.ClusterAgent;
import fr.irit.smac.amas4dc.amas.agent.DataPointAgent;
import fr.irit.smac.amas4dc.cluster.datapoint.DataPoint;
import lombok.Getter;
import lombok.Setter;

import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DynamicClusteringAMAS<T extends DataPoint> extends Amas<DynamicClusteringEnvironment<T>> {
	private static final Logger logger = Logger.getLogger(DynamicClusteringAMAS.class.getName());
	@Setter
	private Consumer<Integer> onCycleEnd;
	@Getter
	private final MASSettings<T> masSettings;

	public DynamicClusteringAMAS(DynamicClusteringEnvironment<T> environment, MASSettings<T> masSettings) {
		super(environment, 1, ExecutionPolicy.TWO_PHASES);
		this.masSettings = masSettings;
	}

	@Override
	protected void onSystemCycleBegin() {
		if (logger.isLoggable(Level.INFO))
			logger.info("---- BEGIN MAS CYCLE ----");
		if (agents.isEmpty() || allAgentsAreReadyToStop()) {
			var newDataPoint = environment.pollPendingDataPoint();
			newDataPoint.ifPresent(dataPoint -> new DataPointAgent<T>(this, dataPoint));
		}
	}

	private boolean allAgentsAreReadyToStop() {
		return agents.stream()
		             .noneMatch(agent -> (agent instanceof ClusterAgent clusterAgent && !clusterAgent.readyToStop()) ||
				             (agent instanceof DataPointAgent dataPointAgent && !dataPointAgent.readyToStop()));
	}

	@Override
	protected void onSystemCycleEnd() {
		if (logger.isLoggable(Level.INFO))
			logger.info("Amount of agents at the end of cycle " + cyclesCount + " : " + agents.size());
		if (this.onCycleEnd != null)
			this.onCycleEnd.accept(agents.size());
	}

	@Override
	public boolean stopCondition() {
		return !environment.hasRemainingPendingAdditionDataPoints() &&
				allAgentsAreReadyToStop();
	}
}
