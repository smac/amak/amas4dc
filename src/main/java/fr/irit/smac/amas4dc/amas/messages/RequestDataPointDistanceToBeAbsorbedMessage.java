package fr.irit.smac.amas4dc.amas.messages;

import fr.irit.smac.amak.messaging.Message;
import fr.irit.smac.amas4dc.amas.agent.DataPointAgent;
import fr.irit.smac.amas4dc.cluster.datapoint.DataPoint;
import lombok.Getter;

public class RequestDataPointDistanceToBeAbsorbedMessage<T extends DataPoint> extends Message<DataPointAgent<T>> {
	@Getter
	private final T dataPoint;

	public RequestDataPointDistanceToBeAbsorbedMessage(DataPointAgent<T> sender, T dataPoint) {
		super(sender);
		this.dataPoint = dataPoint;
	}
}
