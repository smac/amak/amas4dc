package fr.irit.smac.amas4dc.amas.messages;

import fr.irit.smac.amak.messaging.Message;
import fr.irit.smac.amas4dc.amas.agent.ClusterAgent;
import fr.irit.smac.amas4dc.cluster.Cluster;
import fr.irit.smac.amas4dc.cluster.datapoint.DataPoint;
import lombok.Getter;

public class RequestClusterDistanceToBeAbsorbedMessage<T extends DataPoint> extends Message<ClusterAgent<T>> {
	@Getter
	private final Cluster<T> cluster;

	public RequestClusterDistanceToBeAbsorbedMessage(ClusterAgent<T> sender, Cluster<T> cluster) {
		super(sender);
		this.cluster = cluster;
	}
}
