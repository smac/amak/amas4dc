package fr.irit.smac.amas4dc.amas.controller.command;

public record ShutdownCommand() implements Command {
}
