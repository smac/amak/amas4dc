package fr.irit.smac.amas4dc.amas.controller;

import fr.irit.smac.amak.scheduling.Scheduler;
import fr.irit.smac.amas4dc.amas.*;
import fr.irit.smac.amas4dc.amas.agent.ClusterAgent;
import fr.irit.smac.amas4dc.amas.controller.command.NewDataPointCommand;
import fr.irit.smac.amas4dc.amas.controller.command.ShutdownCommand;
import fr.irit.smac.amas4dc.amas.controller.command.SolveCommand;
import fr.irit.smac.amas4dc.amas.controller.query.ExtendedResult;
import fr.irit.smac.amas4dc.amas.controller.query.Result;
import fr.irit.smac.amas4dc.amas.controller.query.RetrieveClustersExtendedResultQuery;
import fr.irit.smac.amas4dc.amas.controller.query.RetrieveClustersResultQuery;
import fr.irit.smac.amas4dc.cluster.datapoint.DataPoint;
import fr.irit.smac.amas4dc.cluster.ExtendedCluster;
import fr.irit.smac.amas4dc.cluster.evaluation.SilhouetteScoreCalculator;
import lombok.Getter;

import java.util.concurrent.Executors;

public class AMAS4DCCommandAndQueryHandler<T extends DataPoint> {
	@Getter
	private final DynamicClusteringEnvironment<T> environment;
	@Getter
	private final MASSettings<T> masSettings;
	@Getter
	private final DynamicClusteringAMAS<T> amas;
	@Getter
	private final Scheduler scheduler;

	public AMAS4DCCommandAndQueryHandler(MASSettings<T> masSettings) {
		this.masSettings = masSettings;
		environment = new DynamicClusteringEnvironment<T>();
		amas = new DynamicClusteringAMAS<T>(environment, masSettings);
		scheduler = new Scheduler(Executors.newSingleThreadExecutor(), amas, environment);
	}

	public void handle(NewDataPointCommand<T> newDataPointCommand) {
		environment.addDataPoints(newDataPointCommand.newDataPoints());
	}

	public void handle(SolveCommand solveCommand) {
		scheduler.startWithSleepSync(0);
	}

	public void handle(ShutdownCommand shutdownCommand) {
		scheduler.stop();
	}

	public Result<T> handle(RetrieveClustersResultQuery retrieveClustersResultQuery) {
		var clusters = amas.getAgents(ClusterAgent.class).stream().map(agent -> ((ClusterAgent<T>) agent).getCluster()).toList();
		return new Result<T>(clusters, masSettings);
	}

	public ExtendedResult<T> handle(RetrieveClustersExtendedResultQuery retrieveClustersResultQuery) {
		var silhouetteScoreCalculator = new SilhouetteScoreCalculator<T>();
		var clusters = amas.getAgents(ClusterAgent.class).stream().map(agent -> (ExtendedCluster<T>)(((ClusterAgent<T>) agent).getCluster())).toList();
		return new ExtendedResult<>(clusters, silhouetteScoreCalculator.compute(masSettings, clusters), masSettings);
	}
}
