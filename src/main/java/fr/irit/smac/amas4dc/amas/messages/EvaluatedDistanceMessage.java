package fr.irit.smac.amas4dc.amas.messages;

import fr.irit.smac.amak.messaging.Message;
import fr.irit.smac.amas4dc.amas.agent.ClusterAgent;
import fr.irit.smac.amas4dc.cluster.Cluster;
import fr.irit.smac.amas4dc.cluster.datapoint.DataPoint;
import lombok.Getter;
import lombok.ToString;

@ToString
public class EvaluatedDistanceMessage<T extends DataPoint> extends Message<ClusterAgent<T>> {
	@Getter
	private final Cluster<T> cluster;
	@Getter
	private final float distance;

	public EvaluatedDistanceMessage(ClusterAgent<T> sender, Cluster<T> cluster, float distance) {
		super(sender);
		this.cluster = cluster;
		this.distance = distance;
	}
}
