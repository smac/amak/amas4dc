package fr.irit.smac.amas4dc.amas.agent;

import fr.irit.smac.amak.Agent;
import fr.irit.smac.amas4dc.amas.DynamicClusteringAMAS;
import fr.irit.smac.amas4dc.amas.DynamicClusteringEnvironment;
import fr.irit.smac.amas4dc.amas.messages.EvaluatedDistanceMessage;
import fr.irit.smac.amas4dc.amas.messages.InformMostSimilarMessage;
import fr.irit.smac.amas4dc.amas.messages.RequestDataPointDistanceToBeAbsorbedMessage;
import fr.irit.smac.amas4dc.cluster.datapoint.DataPoint;
import fr.irit.smac.amas4dc.cluster.distance.ClusterDistance;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DataPointAgent<T extends DataPoint> extends Agent<DynamicClusteringAMAS<T>, DynamicClusteringEnvironment<T>> {
	private static final Logger logger = Logger.getLogger(DataPointAgent.class.getName());
	private final T dataPoint;
	private final ClusterDistancesCollector<T> clusterDistancesCollector = new ClusterDistancesCollector<>();

	public DataPointAgent(DynamicClusteringAMAS<T> amas, T dataPoint) {
		super(amas);
		if (logger.isLoggable(Level.INFO))
			logger.info(this + " created for datapoint " + dataPoint);
		this.dataPoint = dataPoint;
	}


	private boolean isRoughlySimilarTo(ClusterAgent<T> otherClusterAgent) {
		return getAmas().getMasSettings().distanceMethod().areRoughlySimilar(dataPoint, otherClusterAgent.getCluster().getRepresentative());
	}

	@Override
	protected void onReady() {
		super.onReady();
		var clusterAgentsRoughlySimilarOnReady = new ArrayList<ClusterAgent<T>>();
		for (var other : amas.getEnvironment().getClusterAgentsFromBucket(dataPoint.getBucketId())) {
			if (isRoughlySimilarTo(other)) {
				clusterAgentsRoughlySimilarOnReady.add(other);
			}
		}
		for (var agent : clusterAgentsRoughlySimilarOnReady) {
			if (logger.isLoggable(Level.INFO))
				logger.info(this + " sends RequestDataPointDistanceToBeAbsorbedMessage to " + agent);
			agent.getMailbox().receive(new RequestDataPointDistanceToBeAbsorbedMessage<T>(this, dataPoint));
			clusterDistancesCollector.expectClusterDistanceFrom(agent);
		}

		if (logger.isLoggable(Level.INFO))
			logger.info(this + " has " + clusterAgentsRoughlySimilarOnReady.size() + " roughly similar");
	}

	@Override
	protected void onPerceive() {
		while (mailbox.hasMessageOfType(EvaluatedDistanceMessage.class)) {
			var evaluatedDistanceMessage = this.getMailbox().read(EvaluatedDistanceMessage.class);
			if (evaluatedDistanceMessage.isPresent()) {
				clusterDistancesCollector.receiveClusterDistance((ClusterAgent<T>) evaluatedDistanceMessage.get().getSender(),
				                                                 new ClusterDistance<T>(evaluatedDistanceMessage.get().getCluster(), evaluatedDistanceMessage.get().getDistance()));
			}
		}
	}

	@Override
	protected void onDecideAndAct() {
		if (clusterDistancesCollector.areAllReceived()) {
			Pair<Optional<ClusterAgent<T>>, List<ClusterAgent<T>>> mostSimilarAndOthers = clusterDistancesCollector.getMostSimilarAndOthers(amas.getMasSettings().fusionThreshold());
			Optional<ClusterAgent<T>> mostSimilarAgent = mostSimilarAndOthers.first();
			var othersSimilarClusterAgents = mostSimilarAndOthers.second();
			if (mostSimilarAgent.isPresent()) {
				if (logger.isLoggable(Level.INFO))
					logger.info(this + " decides to be absorbed");
				mostSimilarAgent.get().getMailbox().receive(new InformMostSimilarMessage<T>(this,
				                                                                            dataPoint,
				                                                                            othersSimilarClusterAgents));
				destroy();
			} else {
				new ClusterAgent<T>(amas, dataPoint);
				destroy();
			}
		}
	}

	@Override
	protected void onDestroy() {
		if (logger.isLoggable(Level.INFO))
			logger.info(MessageFormat.format("{0} dies", this));
	}

	public boolean readyToStop() {
		return false;
	}

}
