package fr.irit.smac.amas4dc.amas.agent;

import fr.irit.smac.amak.Agent;
import fr.irit.smac.amas4dc.amas.AMASOption;
import fr.irit.smac.amas4dc.cluster.distance.ClusterDistance;
import fr.irit.smac.amas4dc.amas.DynamicClusteringAMAS;
import fr.irit.smac.amas4dc.amas.DynamicClusteringEnvironment;
import fr.irit.smac.amas4dc.amas.messages.EvaluatedDistanceMessage;
import fr.irit.smac.amas4dc.amas.messages.InformMostSimilarMessage;
import fr.irit.smac.amas4dc.amas.messages.RequestDataPointDistanceToBeAbsorbedMessage;
import fr.irit.smac.amas4dc.amas.messages.RequestClusterDistanceToBeAbsorbedMessage;
import fr.irit.smac.amas4dc.cluster.Cluster;
import fr.irit.smac.amas4dc.cluster.datapoint.DataPoint;
import fr.irit.smac.amas4dc.cluster.ExtendedCluster;
import lombok.Getter;

import java.text.MessageFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ClusterAgent<T extends DataPoint> extends Agent<DynamicClusteringAMAS<T>, DynamicClusteringEnvironment<T>> {
	private static final Logger logger = Logger.getLogger(ClusterAgent.class.getName());
	@Getter
	private final Cluster<T> cluster;
	private State state = State.DORMANT;

	private Optional<RequestDataPointDistanceToBeAbsorbedMessage> receivedRequestDataPointDistanceToBeAbsorbedMessage = Optional.empty();
	private Optional<RequestClusterDistanceToBeAbsorbedMessage> receivedRequestClusterDistanceToBeAbsorbedMessage = Optional.empty();
	private Optional<InformMostSimilarMessage> receivedInformMostSimilarClusterAgentsMessage = Optional.empty();
	private final ClusterDistancesCollector<T> clusterDistancesCollector = new ClusterDistancesCollector<>();

	protected ClusterAgent(DynamicClusteringAMAS amas, T dataPoint) {
		super(amas);
		if (logger.isLoggable(Level.INFO))
			logger.info(this + " created");
		if (amas.getMasSettings().amasOptions().contains(AMASOption.KeepAllDataPoints))
			cluster = new ExtendedCluster<T>(dataPoint);
		else
			cluster = new Cluster<T>(dataPoint);
		amas.getMasSettings().database().add(cluster.getRepresentative());
		getAmas().getEnvironment().assignToBucket(cluster.getRepresentative().getBucketId(), this);
	}

	public enum State {DORMANT, WAITING_FOR_DISTANCE, DEAD}

	public State getState() {
		return state;
	}


	@Override
	protected void onAgentCycleEnd() {
		if (state != State.DORMANT)
			if (logger.isLoggable(Level.INFO))
				logger.info(MessageFormat.format("---- End {0} cycle state= {1} ----", this, state));
	}

	public boolean readyToStop() {
		return state == State.DORMANT &&
				!mailbox.hasAMessage();
	}

	@Override
	protected void onPerceive() {
		receivedInformMostSimilarClusterAgentsMessage = Optional.empty();
		receivedRequestDataPointDistanceToBeAbsorbedMessage = Optional.empty();
		receivedRequestClusterDistanceToBeAbsorbedMessage = Optional.empty();
		switch (state) {
			case DORMANT -> {
				this.receivedInformMostSimilarClusterAgentsMessage = this.getMailbox().read(InformMostSimilarMessage.class);
				if (receivedInformMostSimilarClusterAgentsMessage.isPresent()) {
					if (logger.isLoggable(Level.INFO))
						logger.info(this + " received a list of candidates similar cluster agents");
					return;
				}
				this.receivedRequestDataPointDistanceToBeAbsorbedMessage = this.getMailbox().read(RequestDataPointDistanceToBeAbsorbedMessage.class);
				if (receivedRequestDataPointDistanceToBeAbsorbedMessage.isPresent()) {
					if (logger.isLoggable(Level.INFO))
						logger.info(this + " received a request for distance to be absorbed");
					return;
				}
				this.receivedRequestClusterDistanceToBeAbsorbedMessage = this.getMailbox().read(RequestClusterDistanceToBeAbsorbedMessage.class);
				if (receivedRequestClusterDistanceToBeAbsorbedMessage.isPresent()) {
					if (logger.isLoggable(Level.INFO))
						logger.info(this + " received a request for distance to be absorbed");
				}
			}
			case WAITING_FOR_DISTANCE -> {
				while (mailbox.hasMessageOfType(EvaluatedDistanceMessage.class)) {
					var evaluatedDistanceMessage = this.getMailbox().read(EvaluatedDistanceMessage.class);
					if (evaluatedDistanceMessage.isPresent()) {
						clusterDistancesCollector.receiveClusterDistance((ClusterAgent<T>) evaluatedDistanceMessage.get().getSender(),
						                                                 new ClusterDistance<T>(evaluatedDistanceMessage.get().getCluster(), evaluatedDistanceMessage.get().getDistance()));
					}
				}
			}
		}
	}

	@Override
	protected void onDecideAndAct() {
		State nextState = null;
		switch (state) {
			case DORMANT -> {
				if (receivedRequestDataPointDistanceToBeAbsorbedMessage.isPresent()) {
					float distance = computeDistance((T) receivedRequestDataPointDistanceToBeAbsorbedMessage.get().getDataPoint());
					var message = new EvaluatedDistanceMessage<T>(this, cluster, distance);
					var sender = receivedRequestDataPointDistanceToBeAbsorbedMessage.get().getSender();
					if (logger.isLoggable(Level.INFO)) {
						logger.info(this + " --> " + sender + " " + message);
					}
					sender.getMailbox().receive(message);
				} else if (receivedRequestClusterDistanceToBeAbsorbedMessage.isPresent()) {
					float distance = computeDistance((Cluster<T>) receivedRequestClusterDistanceToBeAbsorbedMessage.get().getCluster());
					var message = new EvaluatedDistanceMessage<T>(this, cluster, distance);
					var sender = receivedRequestClusterDistanceToBeAbsorbedMessage.get().getSender();
					if (logger.isLoggable(Level.INFO)) {
						logger.info(this + " --> " + sender + " " + message);
					}
					sender.getMailbox().receive(message);
				} else if (receivedInformMostSimilarClusterAgentsMessage.isPresent()) {
					var informMostSimilarMessage = receivedInformMostSimilarClusterAgentsMessage.get();
					if (informMostSimilarMessage.getDataPoint() != null)
						absorbDataPoint((T) informMostSimilarMessage.getDataPoint());
					else if (informMostSimilarMessage.getCluster() != null)
						absorbCluster(informMostSimilarMessage.getCluster());
					List<ClusterAgent<T>> otherSimilarClusterAgents = ((InformMostSimilarMessage<T>) informMostSimilarMessage).getOtherSimilarClusterAgents();
					for (var otherClusterAgent : otherSimilarClusterAgents) {
						otherClusterAgent.getMailbox().receive(new RequestClusterDistanceToBeAbsorbedMessage(this, cluster));
					}
					if (!otherSimilarClusterAgents.isEmpty()) {
						clusterDistancesCollector.clear();
						clusterDistancesCollector.expectClusterDistanceFrom(otherSimilarClusterAgents);
						nextState = State.WAITING_FOR_DISTANCE;
					} else {
						nextState = State.DORMANT;
					}
				}
			}
			case WAITING_FOR_DISTANCE -> {
				if (clusterDistancesCollector.areAllReceived()) {
					Pair<Optional<ClusterAgent<T>>, List<ClusterAgent<T>>> mostSimilarAndOthers = clusterDistancesCollector.getMostSimilarAndOthers(amas.getMasSettings().fusionThreshold());
					Optional<ClusterAgent<T>> mostSimilarAgent = mostSimilarAndOthers.first();
					var othersSimilarClusterAgents = mostSimilarAndOthers.second();
					if (mostSimilarAgent.isPresent()) {
						if (logger.isLoggable(Level.INFO))
							logger.info(this + " decides to be absorbed");
						mostSimilarAgent.get().getMailbox().receive(new InformMostSimilarMessage<T>(this,
						                                                                            getCluster(),
						                                                                            othersSimilarClusterAgents));
						destroy();
					} else {
						nextState = State.DORMANT;
					}
				}
			}
		}
		if (nextState != null)
			state = nextState;
	}

	private void absorbDataPoint(T otherDataPoint) {
		if (logger.isLoggable(Level.INFO))
			logger.info(this + " absorbs the data point " + otherDataPoint);

		amas.getMasSettings().dataPointFuser().accept(cluster.getRepresentative(), otherDataPoint);
		cluster.addDataPoint(otherDataPoint);
	}

	private void absorbCluster(Cluster<T> otherCluster) {
		if (logger.isLoggable(Level.INFO))
			logger.info(this + " absorbs the cluster " + otherCluster);

		amas.getMasSettings().dataPointFuser().accept(cluster.getRepresentative(), otherCluster.getRepresentative());

		if (getAmas().getMasSettings().amasOptions().contains(AMASOption.KeepAllDataPoints))
			((ExtendedCluster<T>) cluster).addClusterContent((ExtendedCluster<T>) otherCluster);
		else
			cluster.addClusterContent(otherCluster);
		amas.getMasSettings().database().setLastModifiedDataPoint(cluster.getRepresentative());
	}

	@Override
	protected void onDestroy() {
		state = State.DEAD;
		amas.getMasSettings().database().remove(cluster.getRepresentative());
		getAmas().getEnvironment().removeFromBucket(cluster.getRepresentative().getBucketId(), this);
	}


	private float computeDistance(T other) {
		return getAmas().getMasSettings().distanceMethod().apply(this.cluster.getRepresentative(), other);
	}

	private float computeDistance(Cluster<T> other) {
		return getAmas().getMasSettings().distanceMethod().apply(this.cluster.getRepresentative(), other.getRepresentative());
	}
}
