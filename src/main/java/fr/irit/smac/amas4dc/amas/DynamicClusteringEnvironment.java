package fr.irit.smac.amas4dc.amas;

import fr.irit.smac.amak.Environment;
import fr.irit.smac.amas4dc.amas.agent.ClusterAgent;
import fr.irit.smac.amas4dc.cluster.datapoint.DataPoint;
import lombok.Getter;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Logger;

public class DynamicClusteringEnvironment<T extends DataPoint> extends Environment {

	private final Queue<T> pendingAdditionDataPoints = new ConcurrentLinkedQueue<>();
	@Getter
	private T lastPolledPendingDataPoint = null;
	private final Map<Integer, List<ClusterAgent<T>>> buckets = new ConcurrentHashMap<>();

	public boolean hasRemainingPendingAdditionDataPoints() {
		return !pendingAdditionDataPoints.isEmpty();
	}

	public Optional<T> pollPendingDataPoint() {
		var dataPoint = pendingAdditionDataPoints.poll();
		if (dataPoint != null)
			lastPolledPendingDataPoint = dataPoint;
		return Optional.ofNullable(dataPoint);
	}

	public void addDataPoints(List<T> newDataPoints) {
		pendingAdditionDataPoints.addAll(newDataPoints);
	}

	public void assignToBucket(int bucketId, ClusterAgent<T> tClusterAgent) {
		this.buckets.computeIfAbsent(bucketId, b -> Collections.synchronizedList(new ArrayList<>())).add(tClusterAgent);
	}

	public void removeFromBucket(int bucketId, ClusterAgent<T> tClusterAgent) {
		if (!buckets.containsKey(bucketId))
			return;
		this.buckets.get(bucketId).remove(tClusterAgent);
	}

	public List<ClusterAgent<T>> getClusterAgentsFromBucket(int bucketId) {
		return this.buckets.getOrDefault(bucketId, new ArrayList<>());
	}
}
