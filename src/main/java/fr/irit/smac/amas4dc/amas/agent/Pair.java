package fr.irit.smac.amas4dc.amas.agent;

public record Pair<T1, T2>(T1 first, T2 second) {
}
