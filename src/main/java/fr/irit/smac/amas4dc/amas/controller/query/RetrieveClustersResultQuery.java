package fr.irit.smac.amas4dc.amas.controller.query;

public record RetrieveClustersResultQuery() implements Query {
}
