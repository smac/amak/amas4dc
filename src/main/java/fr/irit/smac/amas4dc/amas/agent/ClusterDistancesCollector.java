package fr.irit.smac.amas4dc.amas.agent;

import fr.irit.smac.amas4dc.cluster.datapoint.DataPoint;
import fr.irit.smac.amas4dc.cluster.distance.ClusterDistance;

import java.util.*;

public class ClusterDistancesCollector<T extends DataPoint> {
	private final List<ClusterAgent<T>> responseExpectedAgents = new ArrayList<>();
	private final Map<ClusterAgent<T>, ClusterDistance<T>> receivedClusterDistances = new HashMap<>();

	public void expectClusterDistanceFrom(ClusterAgent<T> agent) {
		responseExpectedAgents.add(agent);
	}

	public void expectClusterDistanceFrom(List<ClusterAgent<T>> otherSimilarClusterAgents) {
		otherSimilarClusterAgents.forEach(this::expectClusterDistanceFrom);
	}

	public void receiveClusterDistance(ClusterAgent<T> sender, ClusterDistance<T> clusterDistance) {
		receivedClusterDistances.put(sender, clusterDistance);
	}

	public boolean areAllReceived() {
		responseExpectedAgents.removeIf(a -> a.getState() == ClusterAgent.State.DEAD && !receivedClusterDistances.containsKey(a));
		for (var key : receivedClusterDistances.keySet()) {
			if (!responseExpectedAgents.contains(key))
				receivedClusterDistances.remove(key);
		}
		return responseExpectedAgents.size() == receivedClusterDistances.size();
	}

	public Pair<Optional<ClusterAgent<T>>, List<ClusterAgent<T>>> getMostSimilarAndOthers(float fusionThreshold) {
		var othersSimilarClusterAgents = new ArrayList<ClusterAgent<T>>();
		Optional<ClusterAgent<T>> mostSimilarClusterAgent = Optional.empty();
		float mostSimilarClusterAgentScore = Float.POSITIVE_INFINITY;
		var allSimilarClusterAgents = new HashMap<ClusterAgent<T>, Double>();
		for (var ssr : receivedClusterDistances.entrySet()) {
			if (ssr.getValue().distance() <= fusionThreshold) {
				if (mostSimilarClusterAgentScore == Float.POSITIVE_INFINITY || ssr.getValue().distance() < mostSimilarClusterAgentScore) {
					mostSimilarClusterAgent = Optional.of(ssr.getKey());
					mostSimilarClusterAgentScore = ssr.getValue().distance();
				}
				allSimilarClusterAgents.put(ssr.getKey(), (double) ssr.getValue().distance());
			}
		}
		for (var ca : allSimilarClusterAgents.keySet()) {
			if (ca != mostSimilarClusterAgent.get())
				othersSimilarClusterAgents.add(ca);
		}
		return new Pair<>(mostSimilarClusterAgent, othersSimilarClusterAgents);

	}

	public void clear() {
		responseExpectedAgents.clear();
		receivedClusterDistances.clear();
	}
}
