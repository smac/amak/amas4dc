package fr.irit.smac.amas4dc.amas.controller.query;

import fr.irit.smac.amas4dc.amas.MASSettings;
import fr.irit.smac.amas4dc.cluster.Cluster;
import fr.irit.smac.amas4dc.cluster.datapoint.DataPoint;

import java.util.List;


public record Result<T extends DataPoint>(List<Cluster<T>> clusters,
                                          int amountOfClustersWithFrequencyEqualToOne,
                                          int amountOfClustersWithFrequencyHigherThan5,
                                          MASSettings<T> masSettings) {
	public Result(List<Cluster<T>> clusters, MASSettings<T> masSettings) {
		this(clusters,
		     (int) clusters.stream().filter(l -> l.getSize() == 1).count(),
		     (int) clusters.stream().filter(l -> l.getSize() > 5).count(),
		     masSettings);
	}
}
