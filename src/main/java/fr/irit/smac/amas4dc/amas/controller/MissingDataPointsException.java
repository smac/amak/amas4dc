package fr.irit.smac.amas4dc.amas.controller;

public class MissingDataPointsException extends RuntimeException {
	public MissingDataPointsException(String s) {
		super(s);
	}
}
