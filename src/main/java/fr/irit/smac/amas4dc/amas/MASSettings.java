package fr.irit.smac.amas4dc.amas;

import fr.irit.smac.amas4dc.cluster.datapoint.DataPoint;
import fr.irit.smac.amas4dc.cluster.datapoint.DataPointDatabase;
import fr.irit.smac.amas4dc.cluster.datapoint.DataPointFuser;
import fr.irit.smac.amas4dc.cluster.distance.DistanceMethod;

import java.util.EnumSet;

public record MASSettings<T extends DataPoint>(
		DistanceMethod<T> distanceMethod,
		float fusionThreshold,
		EnumSet<AMASOption> amasOptions,
		DataPointFuser<T> dataPointFuser,
		DataPointDatabase<T> database
) {
	public MASSettings(DistanceMethod<T> distanceMethod, float fusionThreshold, EnumSet<AMASOption> amasOptions, DataPointFuser<T> dataPointFuser) {
		this(distanceMethod, fusionThreshold, amasOptions, dataPointFuser, new DataPointDatabase<T>());
	}
}
