import fr.irit.smac.amas4dc.AMAS4DC
import fr.irit.smac.amas4dc.amas.AMASOption
import fr.irit.smac.amas4dc.amas.MASSettings
import fr.irit.smac.amas4dc.cluster.datapoint.DataPoint
import fr.irit.smac.amas4dc.cluster.datapoint.DataPointDatabase
import fr.irit.smac.amas4dc.cluster.distance.DistanceMethod
import spock.lang.Specification

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime

class SimpleClusteringIT extends Specification {
	def "Clustering of two similar 1D objects"() {
		def database = new DataPointDatabase<OneDDataPoint>()
		given:
		def amas4dc = new AMAS4DC<OneDDataPoint>(new MASSettings(new OneDDistanceMethod(), 1f, EnumSet.noneOf(AMASOption), (OneDDataPoint dp1, OneDDataPoint dp2) -> {
			return new OneDDataPoint(dp1.value + (dp2.value - dp1.value) * 0.1f)
		}, database))

		when:
		amas4dc.fit([new OneDDataPoint(1), new OneDDataPoint(2)])
		var results = amas4dc.retrieveClusters()

		then:
		database.get().size() == results.clusters().size()
		results.clusters().size() == 1
		results.clusters().get(0).getSize() == 2
	}

	class OneDDataPoint implements DataPoint {
		private final double value
		private final LocalDateTime time

		OneDDataPoint(double value) {
			this.value = value
			this.time = LocalDateTime.now()
		}

		@Override
		LocalTime getEventTime() {
			return this.time.toLocalTime()
		}

		@Override
		LocalDate getEventDate() {
			return this.time.toLocalDate()
		}
	}

	class OneDDistanceMethod implements DistanceMethod<OneDDataPoint> {

		@Override
		float apply(OneDDataPoint dp1, OneDDataPoint dp2) {
			return Math.abs(dp1.value - dp2.value);
		}

		@Override
		boolean areRoughlySimilar(OneDDataPoint dp1, OneDDataPoint dp2) {
			return true
		}
	}
}
