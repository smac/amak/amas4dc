package fr.irit.smac.amas4dc.amas


import fr.irit.smac.amak.messaging.Mailbox
import fr.irit.smac.amas4dc.amas.agent.ClusterAgent
import fr.irit.smac.amas4dc.amas.agent.DataPointAgent
import fr.irit.smac.amas4dc.amas.messages.EvaluatedDistanceMessage
import fr.irit.smac.amas4dc.amas.messages.RequestClusterDistanceToBeAbsorbedMessage
import fr.irit.smac.amas4dc.amas.messages.RequestDataPointDistanceToBeAbsorbedMessage
import fr.irit.smac.amas4dc.cluster.Cluster
import fr.irit.smac.amas4dc.cluster.datapoint.DataPoint
import fr.irit.smac.amas4dc.cluster.datapoint.DataPointFuser
import fr.irit.smac.amas4dc.cluster.distance.DistanceMethod
import spock.lang.Specification
import spock.util.concurrent.BlockingVariable

class ClusterAgentTest extends Specification {

	def "When an agent in DORMANT receives a RequestDataPointDistanceToBeAbsorbedMessage, it should reply with a distance"() {
		given:
		def messageReceivedByRequester = new BlockingVariable()

		def distanceMethodMock = Mock(DistanceMethod)
		distanceMethodMock.apply(_, _) >> 0.3

		def amasMock = Mock(DynamicClusteringAMAS<DataPoint>)
		amasMock.getMasSettings() >> new MASSettings(distanceMethodMock, 0.5f, EnumSet.noneOf(AMASOption), Mock(DataPointFuser))
		amasMock.getEnvironment() >> Mock(DynamicClusteringEnvironment)

		def agent = new ClusterAgent(amasMock, Mock(DataPoint))
		agent.state = ClusterAgent.State.DORMANT

		def requesterMailboxMock = Mock(Mailbox)
		requesterMailboxMock.receive(_) >> { msg ->
			messageReceivedByRequester.set(msg)
		}
		def requesterAgentMock = Mock(DataPointAgent)
		requesterAgentMock.getMailbox() >> requesterMailboxMock

		when:
		agent.mailbox.receive(new RequestDataPointDistanceToBeAbsorbedMessage(requesterAgentMock, Mock(DataPoint)))
		agent.cycle()
		agent.cycle()

		then:
		messageReceivedByRequester.get()[0] instanceof EvaluatedDistanceMessage
		((EvaluatedDistanceMessage) (messageReceivedByRequester.get()[0])).distance == 0.3f
	}
	def "When an agent in DORMANT receives a RequestClusterDistanceToBeAbsorbedMessage, it should reply with a distance"() {
		given:
		def messageReceivedByRequester = new BlockingVariable()

		def distanceMethodMock = Mock(DistanceMethod)
		distanceMethodMock.apply(_, _) >> 0.3

		def amasMock = Mock(DynamicClusteringAMAS<DataPoint>)
		amasMock.getMasSettings() >> new MASSettings(distanceMethodMock, 0.5f, EnumSet.noneOf(AMASOption), Mock(DataPointFuser))
		amasMock.getEnvironment() >> Mock(DynamicClusteringEnvironment)

		def agent = new ClusterAgent(amasMock, Mock(DataPoint))
		agent.state = ClusterAgent.State.DORMANT

		def requesterMailboxMock = Mock(Mailbox)
		requesterMailboxMock.receive(_) >> { msg ->
			messageReceivedByRequester.set(msg)
		}
		def requesterAgentMock = Mock(ClusterAgent)
		requesterAgentMock.getMailbox() >> requesterMailboxMock

		when:
		agent.mailbox.receive(new RequestClusterDistanceToBeAbsorbedMessage(requesterAgentMock, Mock(Cluster)))
		agent.cycle()
		agent.cycle()

		then:
		messageReceivedByRequester.get()[0] instanceof EvaluatedDistanceMessage
		((EvaluatedDistanceMessage) (messageReceivedByRequester.get()[0])).distance == 0.3f
	}
}
