# AMAS4DC

A library for data clustering using multi-agent system

## Set up

* Download AMAS4DC source

* Rename the extracted folder "amas4dc"

* Create a new gradle project in the same directory as AMAS4DC

* In this new project, open build.gradle

* Add AMAS4DC to the dependencies
```
dependencies {

    // Add the two following lines
    // AMAS4DC
    implementation project(':amas4dc')
    
    ...
}
```
* Open settings.gradle

* Add link to local AMAS4DC folder
```
include ':amas4dc'
project(':amas4dc').projectDir = new File(settingsDir, '../amas4dc')
```

* Click on the Gradle Refresh button


## Usage example 

```java
var amas4dc = new AMAS4DC<CustomDataPoint>(new MASSettings(new CustomDataPointDistanceMethod(), 0.5f, EnumSet.noneOf(AMASOption.class), new CustomDataPointFuser()));

amas4dc.fit(List.of(new CustomDataPoint(LocalDateTime.now().minusDays(1), 1.0f), new CustomDataPoint(LocalDateTime.now(), 2.0f)));

var results = amas4dc.retrieveClusters();
System.out.println("Clusters count: " + results.clusters().size());
```

```java
public class CustomDataPoint implements DataPoint {
	
	private final LocalDateTime eventDateTime;
	
	@Getter
	@Setter
	private float value;
	
	public CustomDataPoint(LocalDateTime eventDateTime, float value) {
		this.eventDateTime = eventDateTime;
		this.value = value;
	}
	
	@Override
	public LocalTime getEventTime() {
		return eventDateTime.toLocalTime();
	}
	
	@Override
	public LocalDate getEventDate() {
		return eventDateTime.toLocalDate();
	}
}

public class CustomDataPointFuser implements DataPointFuser<CustomDataPoint> {
	@Override
	public void accept(CustomDataPoint current, CustomDataPoint other) {
		current.setValue((current.getValue() + other.getValue()) / 2);
	}
}

public class CustomDataPointDistanceMethod implements DistanceMethod<CustomDataPoint> {
	@Override
	public float apply(CustomDataPoint dp1, CustomDataPoint dp2) {
		return Math.abs(dp1.getValue() - dp2.getValue());
	}

	@Override
	public boolean areRoughlySimilar(CustomDataPoint dp1, CustomDataPoint dp2) {
		return true;
	}
}
```